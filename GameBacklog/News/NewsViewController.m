//
//  ViewController.m
//  GameBacklog
//
//  Created by yuko on 31/05/18.
//  Copyright © 2018 gb. All rights reserved.
//

#import <SDWebImage/UIImageView+WebCache.h>
#import "NewsViewController.h"
#import "NewTableViewCell.h"
#import "DetailsViewController.h"


@interface NewsViewController ()

@property(nonatomic, readonly) NSMutableArray *newsGame;
@property(nonatomic, readonly) NSString *cellId;
@property(nonatomic) NSMutableArray *filteredGames;
@property(nonatomic) BOOL isFiltered;

@end

@implementation NewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //_newsTableView.rowHeight = UITableViewAutomaticDimension;
    //_newsTableView.estimatedRowHeight = _newsTableView.rowHeight;
    _newsTableView.dataSource = self;
    _newsTableView.delegate = self;
    _cellId = @"newsCellItem";
    _newsGame = [[NSMutableArray alloc] init];
    
    _isFiltered = NO;
    _filteredGames = [[NSMutableArray alloc] init];
    _searchBar.delegate = self;
    
    // https://api-endpoint.igdb.com/games/
    // Headers
    // user-key  6710adf1e140043d5418b2f51330a514
    // Accept    application/json    
    
    _apiURL = [NSURL URLWithString: @"https://api-endpoint.igdb.com/games/?fields=id,name,rating,cover,summary&limit=18&order=popularity:desc"];
    NSURLRequest *request = [NSURLRequest requestWithURL:_apiURL];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"user-key": @"6710adf1e140043d5418b2f51330a514",
                                                   @"Accept": @"application/json"};
    NSURLSession *session = [NSURLSession sessionWithConfiguration: sessionConfiguration];
    NSURLSessionDataTask *downloadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error download, %@", error.localizedDescription);
        } else {
            NSError *jsonError = nil;
            NSDictionary *gameList = [NSJSONSerialization JSONObjectWithData: data
                                                                     options: 0
                                                                       error: &jsonError];
            if (jsonError) {
                NSLog(@"Error json, %@", jsonError.localizedDescription);
            } else {
                for (NSDictionary *game in gameList) {
                    [self->_newsGame addObject:game];
                }
                NSLog(@"%@", gameList);
                [self->_newsTableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
            }
        }
    }];
    
    [downloadTask resume];
    
    // TODO NSFetchedResultsController    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_isFiltered) {
        return _filteredGames.count;
    }
    return _newsGame.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:_cellId];
    if (!cell) {
        cell = [[NewTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:_cellId];
    }
    if (_isFiltered) {
        cell.titleLabel.text = _filteredGames[indexPath.row][@"name"];
        cell.descriptionLabel.text = _filteredGames[indexPath.row][@"summary"];
        /*NSNumber *score = _filteredGames[indexPath.row][@"score"];
        if (score) {
            cell.scoreLabel.text = [NSString stringWithFormat:@"%.1f", score.doubleValue];
        } else {
            cell.scoreLabel.text = @"-";
        }*/
        NSString *url = [NSString stringWithFormat:@"https:%@", _filteredGames[indexPath.row][@"cover"][@"url"]];
        [cell.cover sd_setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"placeholder"]];
    } else {
        cell.titleLabel.text = _newsGame[indexPath.row][@"name"];
        cell.descriptionLabel.text = _newsGame[indexPath.row][@"summary"];
        NSNumber *score = _newsGame[indexPath.row][@"score"];
        if (score) {
            cell.scoreLabel.text = [NSString stringWithFormat:@"%.1f", score.doubleValue];
        } else {
            cell.scoreLabel.text = @"-";
        }
        NSString *url = [NSString stringWithFormat:@"https:%@", _newsGame[indexPath.row][@"cover"][@"url"]];
        NSLog(@"%@", url);
        [cell.cover sd_setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"placeholder"]];
    }
    return cell;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length != 0) {
        _isFiltered = YES;
        for (NSDictionary *game in _newsGame) {
            NSRange range = [game[@"name"] rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if (range.location != NSNotFound) {
                [_filteredGames addObject:game];
            }
        }
    } else {
        _isFiltered = NO;
        if (_filteredGames.count > 0) {
            [_filteredGames removeAllObjects];
        }
    }
    [_newsTableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showDetails" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetails"]) {
        NSIndexPath *index = [_newsTableView indexPathForSelectedRow];
        DetailsViewController *detailsVC = [segue destinationViewController];
        detailsVC.gameId = _newsGame[index.row][@"id"];
        detailsVC.gameTitle = _newsGame[index.row][@"name"];
        detailsVC.gameDescription = _newsGame[index.row][@"summary"];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
