//
//  ViewController.h
//  GameBacklog
//
//  Created by yuko on 31/05/18.
//  Copyright © 2018 gb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsViewController : UIViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *newsTableView;

@property (nonatomic, readonly) NSURL *apiURL;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@end
