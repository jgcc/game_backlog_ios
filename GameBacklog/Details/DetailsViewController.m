//
//  DetailsViewController.m
//  GameBacklog
//
//  Created by yuko on 28/06/18.
//  Copyright © 2018 gb. All rights reserved.
//

#import "DetailsViewController.h"

@interface DetailsViewController ()

@end

@implementation DetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"%@, %@, %@", _gameId, _gameTitle, _gameDescription);
    NSURL *apiUrl = [NSURL URLWithString:
                     [NSString stringWithFormat:@"https://api-endpoint.igdb.com/games/%@?fields=*", _gameId]];
    NSURLRequest *request = [NSURLRequest requestWithURL:apiUrl];
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.HTTPAdditionalHeaders = @{@"user-key": @"6710adf1e140043d5418b2f51330a514",
                                                   @"Accept": @"application/json"};
    NSURLSession *session = [NSURLSession sessionWithConfiguration: sessionConfiguration];
    NSURLSessionDataTask *downloadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"Error download, %@", error.localizedDescription);
        } else {
            NSError *jsonError = nil;
            NSDictionary *gameList = [NSJSONSerialization JSONObjectWithData: data
                                                                     options: 0
                                                                       error: &jsonError];
            if (jsonError) {
                NSLog(@"Error json, %@", jsonError.localizedDescription);
            } else {
                NSLog(@"%@", gameList);
            }
        }
    }];
    
    [downloadTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)AddToBacklog:(UIButton *)sender {
    
}

@end
