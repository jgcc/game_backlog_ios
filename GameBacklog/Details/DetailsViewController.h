//
//  DetailsViewController.h
//  GameBacklog
//
//  Created by yuko on 28/06/18.
//  Copyright © 2018 gb. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailsViewController : UIViewController

@property(nonatomic) NSNumber *gameId;
@property(nonatomic) NSString *gameTitle;
@property(nonatomic) NSString *gameDescription;

@end
