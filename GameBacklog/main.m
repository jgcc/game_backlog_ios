//
//  main.m
//  GameBacklog
//
//  Created by yuko on 31/05/18.
//  Copyright © 2018 gb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
